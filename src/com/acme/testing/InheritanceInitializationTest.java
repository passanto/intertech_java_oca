package com.acme.testing;

public class InheritanceInitializationTest {
    public static void main(String[] args) {
        Instructor i = new Instructor();
        i.sayHello();
        System.out.println(Instructor.str1);
    }
}

class Person {
    {
        System.out.println("Person: First Instance Initialization Block");
    }

    static {
        System.out.println("Person: First Static Block");
    }

    {
        System.out.println("Person: Second Instance Initialization Block");
    }

    static {
        System.out.println("Person: Second Static Block");
    }

    public Person() {
        System.out.println("Person()");
    }

    public void sayHello() {
        System.out.println("Person: Hello!");
    }
}

class Employee extends Person {
    {
        System.out.println("Employee: First Instance Initialization Block");
    }

    static {
        System.out.println("Employee: First Static Block");
    }

    {
        System.out.println("Employee: Second Instance Initialization Block");
    }

    static {
        System.out.println("Employee: Second Static Block");
    }

    public Employee() {
        System.out.println("Employee()");
    }
}

class Instructor extends Employee {
    {
        System.out.println("Instructor: First Instance Initialization Block");
        str1 = "First Instance Initialization String";
    }

    static {
        System.out.println("Instructor: First Static Block");
        str1 = "First Static Initialization String";
    }

    public static String str1 = "Explicit Initialization String";

    {
        System.out.println("Instructor: Second Instance Initialization Block");
        str1 = "Second Instance Initialization String";
    }

    static {
        System.out.println("Instructor: Second Static Block");
        str1 = "Second Static Initialization String";
    }

    public Instructor() {
        System.out.println("Instructor()");
    }
}