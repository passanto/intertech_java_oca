package com.acme.testing;

import com.acme.domain.Entity;
import com.acme.domain.Person;
import com.acme.utils.MyDate;

public class CovarianceTest {
    public static void main(String[] args) {
        PersonRegistry pr = new PersonRegistry();
        // Covariance allow to override method
        // and return a SubType of the overridden
        // return type
        //
        // This way one can be more specific
        Person p = pr.whoAmi("person");
    }
}

class EntityRegistry {
    public Entity whoAmi(String input) {
        System.out.println("Returning an Entity from EntityRegistry");
        return new Entity();
    }
}

class PersonRegistry extends EntityRegistry {
    public Person whoAmi(String input) {
        System.out.println("Returning a Person from PersonRegistry");
        return new Person("Joe", "Doe", new MyDate());
    }
}
