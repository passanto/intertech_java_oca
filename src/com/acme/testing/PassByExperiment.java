package com.acme.testing;

import com.acme.utils.MyDate;

public class PassByExperiment {

    public static void passObject(MyDate d) {
        d.setYear(2009);
    }

    public static void passPrimitive(int i) {
        i = 2010;
    }

    public static void passString(String s) {
        int yearSlash = s.lastIndexOf('/');
        s = s.substring(0, yearSlash + 1);
        s += "2012";
        System.out.println("New date string: " + s);
    }

    public static void passStringBuilder(StringBuilder sb) {
        int yearSlash = sb.toString().lastIndexOf('/');
        //StringBuilder sb = new StringBuilder();
        sb.append(sb.substring(0, yearSlash + 1));
        sb.append("2012");
        System.out.println("New date StringBuilder: " + sb);
    }

    public static void main(String[] args) {
        MyDate date = new MyDate(1, 12, 2008);

        System.out.println("Before passing an object " + date);
        passObject(date);
        System.out.println("After passing an object " + date);
        System.out.println();
        // 2009

        System.out.println("Before passing a primitive " + date.getYear());
        passPrimitive(date.getYear());
        System.out.println("After passing a primitive " + date.getYear());
        System.out.println();
        // 2009

        String x = date.toString();
        System.out.println("Before passing a String " + x);
        passString(x);
        System.out.println("After passing a String " + x);
        System.out.println();
        // 2009

        StringBuilder sb = new StringBuilder(date.toString());
        System.out.println("Before passing a StringBuilder " + sb.toString());
        passStringBuilder(sb);
        System.out.println("After passing a StringBuilder " + sb.toString());
        System.out.println();
        // 2009
    }
}
