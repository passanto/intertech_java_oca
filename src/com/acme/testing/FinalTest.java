package com.acme.testing;

public class FinalTest {

    //
    // FINAL VARIABLES:
    // CONSTANTS
    //

    // explicit initialization
    public static final int LESS_TRAVELLED = 0;


    public final int SHORTEST;

    public FinalTest() {
        // constructor initialization
        SHORTEST = 1;

        // illegal
        // final variables (constants) cannot change
        //
        // LESS_TRAVELLED=2;
    }

    public static void main(String[] args) {
        // illegal
        // final variables (constants) cannot change
        //
        // this.SHORTEST=2;
    }
}


//
// FINAL METHODS:
// UN-OVERRIDABLE
//
class Super {
    public final void drive() {
        System.out.println("driving, finally...");
    }
}

class Sub extends Super {
    // illegal
    // final methods cannot be overridden
    //
    //public final void drive() {
    //    System.out.println("sub driving, finally...");
    //}
}


//
// FINAL CLASSES:
// UN-EXTENDABLE
//
// String is a final class
//
// Enum constants are implicitly static final
//

final class SuperFinal {
}

// illegal
// final classes cannot be extended
//
// class SubFinal extends SuperFinal {
// }
