package com.acme.testing;

import com.acme.domain.Employee;
import com.acme.domain.Person;
import com.acme.utils.MyDate;

public class OverridingTester {

    public static void main(String[] args) {
        Person p = new Employee("Joe", "Doe", new MyDate());
        p.sayHello(); // STATIC: hello from super Person

        // here, since a static method is getting called
        // the JVM uses the reference Object,
        // which is the super Person

        // now there is an actual instance method
        // and there is an actual instance of Employee
        p.sayHi(); // INSTANCE: hi from super Employee

        // super method
        ((Employee) p).driveAsPerson();

        // super "super" method
        ((Employee) p).driveAsEntity();
    }

}
