package com.acme.domain;


import com.acme.utils.MyDate;

public class Order {

    // instance variables
    MyDate orderDate;
    double orderAmount = 0.00;
    String customer;
    String product;
    int quantity;

    public char jobSize() {
        char size = 'S';
        if (quantity > 26 & quantity <= 75) {
            size = 'M';
        } else if (quantity > 76 & quantity <= 150) {
            size = 'L';
        } else if (quantity > 150) {
            size = 'X';
        }
        return size;
    }

    public int getDiscountPerc() {
        int d = 0;
        if (jobSize() == 'M') {
            d = 1;
        } else if (jobSize() == 'L') {
            d = 2;
        } else if (jobSize() == 'X') {
            d = 3;
        }
        return d;
    }


    public double computeTotal() {
        return orderAmount - (getDiscountPerc() * orderAmount) +
                ((orderAmount > 150) ? 0 : computeTax());
    }


    // class variables
    public static double taxRate = 0.05;


    // class methods
    public static void setTaxRate(double newRate) {
        taxRate = newRate;
    }

    public static void computeTaxOn(double anAmount) {
        System.out.println("The tax for " + anAmount + " is: " +
                anAmount
                        * Order.taxRate);
    }

    public Order(MyDate d, double amt, String c, String p, int q) {
        orderDate = d;
        orderAmount = amt;
        customer = c;
        product = p;
        quantity = q;
    }

    // instance methods
    public String toString() {
        return quantity + " ea. " + product + " for " + customer;
    }

    public double computeTax() {
        System.out.println("The tax for this order is: " +
                orderAmount
                        * Order.taxRate);
        return orderAmount * Order.taxRate;
    }
}