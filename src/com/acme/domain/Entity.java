package com.acme.domain;

public class Entity {

    void drive() throws RuntimeException {
        System.out.println("I'm an entity driving");
    }
}
