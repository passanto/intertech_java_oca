package com.acme.domain;

import com.acme.utils.MyDate;

public class Employee extends Person {

    private MyDate hireDate;
    private int salary;


    public Employee(String firstName,
                    String lastName,
                    MyDate birthDate,
                    MyDate hireDate,
                    int salary) {
        // there are different options to use this constructor

        // 1
        // using the super (Person) constructor
        // (standard way)
        // (has to be the first method call in the constructor)
        super(firstName, lastName, birthDate);

        // 2
        // using the super-matching constructor
        // this(firstName, lastName, birthDate);

        // 3
        // add the default constructor to super (Person) class

        // 4
        // using super setters
        // (rarely used)
        // setFirstName(firstName);
        // setLastName(lastName);
        // setBirthDate(birthDate);


        this.hireDate = hireDate;
        this.salary = salary;
    }

    // super-matching (Parson-matching) constructor
    public Employee(String firstName, String lastName, MyDate birthDate) {
        super(firstName, lastName, birthDate);
    }

    public void promote(int percentage) {
        this.salary *= 1 + (percentage / 100);
    }

    // OVERRIDING:
    //
    // 1
    // modifier has to be the same or more accessible
    // of the super class' method
    //
    // 2
    // cannot throw more exception than the overridden one
    // unless they are subtype (of those exceptions)
    // or a RuntimeException
    //
    // 3
    // static nor private methods can be overridden
    public void drive() throws RuntimeException {
        System.out.println("I'm an employee driving");
    }

    public void driveAsPerson() throws RuntimeException {
        System.out.println("I'm an Employee but now I'm gonna drive like a Person:");
        super.drive();
    }

    public void driveAsEntity() throws RuntimeException {
        System.out.println("I'm an Employee but now I'm gonna drive like an Entity:");
        // illegal
        //super.super.drive();
        super.driveAsEntity();
    }

    public static void sayHello() {
        System.out.println("STATIC: hello from super Employee");
    }

    public void sayHi() {
        System.out.println("INSTANCE: hi from super Employee");
    }


}
