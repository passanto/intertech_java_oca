package com.acme.domain;

class Account {
    public double balance = 100.0;
}

public class Bank {
    public static void main(String[] args) {

        Account myAcct = new Account();
        double proposedLoan = 250.00;
        String customerName = "Jason Shapiro";

        discountLoan(proposedLoan);
        System.out.println(proposedLoan);

        debitFee(myAcct);
        System.out.println(myAcct.balance);

        salutation(customerName);
        System.out.println(customerName);
    }

    private static void salutation(String aName) {
        aName = "Dear " + aName;
        System.out.println("aName in salutation is: " + aName);
        /*
         Since Strings are immutable, aName now refers to
         a new String.
         the original String, which customerName refers to,
         has still the original value
         */
    }

    private static void debitFee(Account acct) {
        acct.balance = acct.balance - 2.50;
    }

    private static void discountLoan(double x) {
        x = x * 0.95;
    }
}
