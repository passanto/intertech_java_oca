package com.acme.domain;

import com.acme.utils.MyDate;

public class Person extends Entity{
    private String firstName;
    private String lastName;
    private MyDate birthDate;

    public Person(String firstName,
                  String lastName,
                  MyDate birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public MyDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(MyDate birthDate) {
        this.birthDate = birthDate;
    }

    // OVERRIDDEN
    // (exception types for example)
    void drive() throws ArrayIndexOutOfBoundsException{
        System.out.println("I'm a person driving");
    }

    public static void sayHello() {
        System.out.println("STATIC: hello from super Person");
    }

    public void sayHi() {
        System.out.println("INSTANCE: hi from super Person");
    }


    protected void driveAsEntity() {
        System.out.println("I'm a Person but now I'm gonna drive like an Entity:");
        super.drive();
    }
}
