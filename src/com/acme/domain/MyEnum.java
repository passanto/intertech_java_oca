package com.acme.domain;

public enum MyEnum {
    ENUM_1(11), ENUM_2(2);

    private int i;
    private MyEnum(int i){
        this.i=i;
    }

    public String toString(){
        return this.name() + ": " + this.ordinal()+ " - i:" + this.i ;
    }

}
