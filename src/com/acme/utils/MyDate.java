package com.acme.utils;

public class MyDate {
    private int day;
    private int year;
    private int month;

    {
        this.day = 1;
        this.month = 1;
        this.year = 2000;
    }

    public MyDate() {
        this(1, 1, 1900);
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.year = year;
        this.month = month;
    }

    public void setDate(int m, int d, int y) {
        if (valid(d, m, y)) {
            this.month = m;
            this.day = d;
            this.year = y;
        }
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (valid(day, month, year)) {
            this.day = day;
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (valid(day, month, year)) {
            this.year = year;
        }
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (valid(day, month, year)) {
            this.month = month;
        }
    }

    public static void leapYears() {
        for (int y = 1752; y <= 2020; y++) {
            if (y % 4 == 0) {
                System.out.println(y + " is a leap year");
            }
        }
    }

    private boolean valid(int day, int month, int year) {
        if (day > 31 || day < 1 || month > 12 || month < 1 || year < 0) {
            System.out.println("Attempting to create a non-valid date " + month + "/" + day + "/" + year);
            return false;
        }
        switch (month) {
            case 4:
            case 6:
            case 9:
            case 11:
                return (day <= 30);
            case 2:
                return day <= 28 || (day == 29 && year % 4 == 0);

        }
        return true;
    }


    @Override
    public String toString() {
        return month + "/" + day + "/" + year;
    }
}
