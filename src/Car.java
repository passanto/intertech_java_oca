public class Car {


    // references are created on instantiation.
    // their eventual values later
    String color;
    String type;
    int serialNumber;
    static int carCount;

    // initialization block:
    // - gets executed by ALL the constructors
    // - its position determine when it gets executed upon instantiation
    {
        color = "none";
    }

    void start() {
        System.out.println("Get Started!");
    }

    // static initialization block
    public static void setCarCount(int c) {
        carCount = c;
    }

    void printDescription() {
        System.out.println("This is a " + color + " " + type);
    }

    public static void main(String[] args) {
        Car myCar = new Car();
        myCar.printDescription();
    }

    Car() {
        carCount++;
        serialNumber = carCount;
    }
    void driveCar() {
        String greeting = null;
        System.out.println(greeting + " Would you like to drive my "
                + color + " " + type + "?");
    }
}
